package com.hascode.tutorial;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {

    static class Person {

        String name;
        int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return this.name;
        }

        public int getAge() {
            return this.age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Predicate<Person> olderThanFifty = p -> p.getAge() > 50;
        Predicate<Person> teenager = p -> p.getAge() < 20 && p.getAge() >= 10;

        Person p1 = new Person("Tim", 13);
        Person p2 = new Person("Lisa", 52);
        Person p3 = new Person("Bart", 9);
        Person p4 = new Person("Mitch", 14);
        Person p5 = new Person("Beth", 64);

        List<Person> persons = new ArrayList<>();
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);
        persons.add(p5);

        System.out.println("--- experienced ---");
        persons.stream().filter(olderThanFifty).forEach(p -> System.out.println(p.getName()));

        System.out.println("--- teenagers ---");
        List<Person> teens = persons.stream().filter(teenager).collect(Collectors.<Person>toList());
        teens.forEach(t -> System.out.println(t.getName()));

        List<String> namesBeginningWithLetterB = persons.stream().filter(p -> p.getName().startsWith("B")).map(p -> p.getName()).collect(Collectors.<String>toList());
        System.out.println("--- names beginning with 'B' ---");
        namesBeginningWithLetterB.forEach(name -> {
            System.out.println(name);
        });

        System.out.println("--- thread/runnable example ---");
        new Thread(() -> System.out.println("I run")).start();

        Thread.sleep(2000);
        System.out.println("--- parallel/aggregates example ---");
        long amountTeens = persons.parallelStream().filter(teenager).count();
        System.out.println(amountTeens + " teens found");

        System.out.println("--- additional aggregates example ---");
        int min = persons.stream().min((pers1, pers2) -> Integer.compare(pers1.getAge(), pers2.getAge())).get().getAge();
        int max = persons.stream().max((pers1, pers2) -> Integer.compare(pers1.getAge(), pers2.getAge())).get().getAge();
        System.out.println("min age: " + min + ", max age: " + max);
        int cumulatedYears = persons.stream().mapToInt(p -> p.getAge()).sum();
        System.out.println("all persons have a cumulated age of " + cumulatedYears);
        long yearsMultiplied = persons.stream().mapToInt(p -> p.getAge()).reduce(1, (x, y) -> x * y);
        System.out.println("years multiplied " + yearsMultiplied);

        System.out.println("--- sorting example ---");
        persons.stream().sorted((pers1, pers2) -> pers1.getName().compareTo(pers2.getName())).forEach(p -> System.out.println("name: " + p.getName() + ", age: " + p.getAge()));

        System.out.println("--- optional examples ---");
        Optional<Person> person = computePossiblyWithoutResult(true);
        if (person.isPresent()) {
            System.out.println(person.get().getName());
        }
        // or better:
        person.ifPresent(p -> System.out.println(p.getName()));

        // with fallback
        Person fallbackPerson = new Person("Max", 33);
        System.out.println(person.orElse(fallbackPerson).getName());
        person = computePossiblyWithoutResult(false);
        System.out.println(person.orElse(fallbackPerson).getName());

        System.out.println("--- @FunctionalInterface example ---");
        execFoo((msg) -> System.out.println(msg));

        System.out.println("--- special method references #1---");
        Predicate<String> namesWithBFilter = namesBeginningWithLetterB::contains;
        List<String> otherNames = new ArrayList<>();
        otherNames.add("Zed");
        otherNames.add("Teddy");
        otherNames.add("Beth");
        otherNames.add("Jess");
        otherNames.stream().filter(namesWithBFilter).forEach(name -> {
            System.out.println(name);
        });

        System.out.println("--- special method references #2 ---");
        Comparator<Person> byNameComparator = Comparator.comparing(Person::getName);
        Collections.sort(persons, byNameComparator);
        persons.forEach(p -> System.out.println(p.getName()));

    }

    private static Optional<Person> computePossiblyWithoutResult(boolean doesWork) {
        if (doesWork) {
            Person p = new Person("Sam I am", 1);
            return Optional.of(p);
        }

        return Optional.empty();
    }

    @FunctionalInterface
    interface Foo {

        void execute(String msg);
    }

    public static void execFoo(Foo foo) {
        foo.execute("test");
    }

}
